import psutil, subprocess
from time import sleep

# =================================================================
# VARIABLES
# =================================================================
runner = 'podman'
init_c = 5
num_survey = 4
# =================================================================

class metrics:
    def __init__(self):
        self.containers = int(subprocess.run([count_command], shell=True, capture_output=True).stdout)
        self.cpu = psutil.cpu_percent(5)
        self.ram = psutil.virtual_memory().used / 1024 / 1024

def create_containers(number, runner):
    for i in range(number):
        subprocess.run([run_command], shell=True, check=True)
    sleep(5)

stop_command = runner + ' stop $(' + runner + ' ps -q) > /dev/null'
count_command = runner + ' ps -q | wc -l'
run_command = runner + ' run --rm -d nginx:1.19-alpine > /dev/null'

existing_containers = int(subprocess.run([count_command], shell=True, capture_output=True).stdout)
data = []

if existing_containers != 0:
    print('Removing existing containers...')
    subprocess.run([stop_command], shell=True, check=True)

data.append(metrics())
print('Creating', init_c, 'containers...')
create_containers(init_c, runner)
data.append(metrics())
for num in range(num_survey):
    print('Creating', init_c, 'more containers...')
    create_containers(init_c, runner)
    data.append(metrics())
    init_c = init_c * 2

print('\nRESULTS')
print('================================')
for item in data:
    print('Number of containers:', item.containers)
    print('CPU pct:\t', item.cpu)
    print('Mem usage:\t', item.ram)
    print('================================')
